Atos

## Version

- Java 15.0.3

## Start MongoDB

```
docker-compose up -d
```

## Build and run the appplication

```
./gradlew build
java -jar build/libs/Atos-0.0.1-SNAPSHOT.jar
```

## Access to the documentation api

http://localhost:8080/swagger

You will see all access points, and you will be able to test them

## Gradle dependencies
```
io.springfox:springfox-swagger2:2.9.2
io.springfox:springfox-swagger-ui:2.9.2
org.springframework.boot:spring-boot-starter
org.springframework.boot:spring-boot-starter-data-mongodb
org.springframework.boot:spring-boot-starter-web
org.springframework.boot:spring-boot-starter-validation

testorg.springframework.boot:spring-boot-starter-test

org.projectlombok:lombok:1.18.6
org.projectlombok:lombok:1.18.6
```
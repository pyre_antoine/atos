package com.example.atos.service;

import com.example.atos.controller.user.dto.FormUser;
import com.example.atos.controller.user.dto.UserDTO;
import com.example.atos.dao.UserDAO;
import com.example.atos.exceptions.InvalidCredentialsException;
import com.example.atos.exceptions.NotFoundException;
import com.example.atos.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    private UserService userService;
    @Mock
    private UserRepository mockUserRepository;

    @BeforeEach
    public void setUp() {
        userService = new UserService(mockUserRepository);
    }

    @Test
    void should_throw_InvalidCredentialsException() {
        FormUser formUser = new FormUser();

        assertThrows(InvalidCredentialsException.class, () -> {
            userService.createUser(formUser);
        });
    }

    @Test
    void should_create_user() {
        FormUser formUser = new FormUser();
        formUser.setAge(UserService.MIN_AGE);
        formUser.setCountry(UserService.REQUIRED_COUNTRY);
        formUser.setLastName("LASTNAME");
        formUser.setFirstName("FIRSTNAME");
        formUser.setNickName("NICKNAME");

        UserDAO userDAO = new UserDAO();
        userDAO.setAge(formUser.getAge());
        userDAO.setFirstName(formUser.getFirstName());
        userDAO.setLastName(formUser.getLastName());
        userDAO.setNickName(formUser.getNickName());

        when(mockUserRepository.save(any())).thenReturn(userDAO);
        try {
            UserDTO userDTO = userService.createUser(formUser);

            assertEquals(formUser.getAge(), userDTO.getAge());
            assertEquals(formUser.getFirstName(), userDTO.getFirstName());
            assertEquals(formUser.getLastName(), userDTO.getLastName());
            assertEquals(formUser.getNickName(), userDTO.getNickName());
        } catch (InvalidCredentialsException e) {
            e.printStackTrace();
        }
    }

    @Test
    void should_throw_NotFoundException() {
        when(mockUserRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> {
            userService.findUser("ID");
        });
    }
    @Test
    void should_return_user() {
        String ID = "ID";

        UserDAO userDAO = new UserDAO();
        userDAO.setId(ID);

        when(mockUserRepository.findById(any())).thenReturn(Optional.empty());

        try {
            UserDTO userDTO = userService.findUser(ID);

            assertEquals(ID, userDTO.getId());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }
}
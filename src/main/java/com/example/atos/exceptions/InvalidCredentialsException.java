package com.example.atos.exceptions;

public class InvalidCredentialsException extends Exception {

    public InvalidCredentialsException(String message) {super(message);}
}

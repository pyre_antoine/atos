package com.example.atos.exceptions;

public class NotFoundException extends Exception {

    public NotFoundException(String message) {super(message);}
}

package com.example.atos.controller.user.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class FormUser {

    @Min(18)
    private Integer age;
    @NotEmpty
    private String country;

    //Optional fields
    private String firstName;
    private String lastName;
    private String nickName;

    public String toString() {
        return age.toString() + " " + country + " " + firstName + " " + lastName + " " + nickName;
    }
}

package com.example.atos.controller.user;

import com.example.atos.controller.user.dto.FormUser;
import com.example.atos.controller.user.dto.UserDTO;
import com.example.atos.exceptions.InvalidCredentialsException;
import com.example.atos.exceptions.NotFoundException;
import com.example.atos.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    @ApiOperation(value= "Register a new user",
            response= UserDTO.class,
            notes = "Country has to be France, not case sensitive. Age has to be higher than 18"
    )
    @ApiResponses(value= {
            @ApiResponse(code= 201, message = "Created"),
            @ApiResponse(code= 400, message = "Invalid form")
    })
    public ResponseEntity<UserDTO> register(@RequestParam String country,
                                            @RequestParam int age,
                                            @RequestParam(defaultValue = "unknown", required = false) String nickName,
                                            @RequestParam(required = false) String firstName,
                                            @RequestParam(required = false) String lastName
    ) {
        Date startedAt = new Date();
        FormUser formUser = new FormUser();
        formUser.setCountry(country);
        formUser.setAge(age);
        formUser.setFirstName(firstName);
        formUser.setLastName(lastName);
        formUser.setNickName(nickName);

        log.info("Input : " + formUser);

        try {
            UserDTO userDTO = userService.createUser(formUser);
            log.info("Output : " + userDTO.toString());
            log.info("Processing time : " + (new Date().getTime() - startedAt.getTime()) + "ms");
            return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
        } catch (InvalidCredentialsException e) {
            log.info("InvalidCredentialsException");
            log.info("Processing time : " + (new Date().getTime() - startedAt.getTime()) + "ms");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @GetMapping()
    @ApiOperation(value= "List all users",
            response= UserDTO.class,
            responseContainer = "List"
    )
    @ApiResponses(value= {
            @ApiResponse(code= 200, message = "OK")
    })
    public ResponseEntity<List<UserDTO>> findAll() {
        Date startedAt = new Date();

        List<UserDTO> userDTOS = userService.findAllUsers();

        log.info("User amount : " + userDTOS.size());
        log.info("Processing time : " + (new Date().getTime() - startedAt.getTime()) + "ms");
        return ResponseEntity.ok().body(userDTOS);
    }

    @GetMapping("/{userId}")
    @ApiOperation(value= "Find a specific user",
            response= UserDTO.class
    )
    @ApiResponses(value= {
            @ApiResponse(code= 200, message = "Found"),
            @ApiResponse(code= 404, message = "Not Found")
    })
    public ResponseEntity<UserDTO> findUser(@PathVariable("userId") String userId) {
        Date startedAt = new Date();
        log.info("Input : " + userId);

        try {
            UserDTO userDTO = userService.findUser(userId);
            log.info("Output : " + userDTO.toString());
            log.info("Processing time : " + (new Date().getTime() - startedAt.getTime()) + "ms");
            return ResponseEntity.ok().body(userDTO);
        } catch (NotFoundException e) {
            log.info("NotFoundException");
            log.info("Processing time : " + (new Date().getTime() - startedAt.getTime()) + "ms");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}

package com.example.atos.controller.user.dto;

import com.example.atos.dao.UserDAO;
import lombok.Data;

@Data
public class UserDTO {

    private String id;
    private Integer age;
    private String firstName;
    private String lastName;
    private String nickName;

    public UserDTO(UserDAO userDAO) {
        this.id = userDAO.getId();
        this.age = userDAO.getAge();
        this.firstName = userDAO.getFirstName();
        this.lastName = userDAO.getLastName();
        this.nickName = userDAO.getNickName();
    }

    public String toString() {
        return id + " " + age.toString() + " " + firstName + " " + lastName + " " + nickName;
    }
}

package com.example.atos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/swagger")
public class SwaggerController {
    @GetMapping
    public String swagger() {
        return "redirect:/swagger-ui.html";
    }
}

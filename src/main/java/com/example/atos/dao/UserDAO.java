package com.example.atos.dao;

import com.example.atos.controller.user.dto.FormUser;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "user")
public class UserDAO {

    public UserDAO() {

    }

    public UserDAO(FormUser formUser) {
        this.age = formUser.getAge();
        this.firstName = formUser.getFirstName();
        this.lastName = formUser.getLastName();
        this.nickName = formUser.getNickName();
    }

    @Id
    private String id;

    //Mandatory field;
    private Integer age;

    //Optional fields
    private String firstName;
    private String lastName;
    private String nickName;
}

package com.example.atos.service;

import com.example.atos.controller.user.dto.FormUser;
import com.example.atos.controller.user.dto.UserDTO;
import com.example.atos.dao.UserDAO;
import com.example.atos.exceptions.InvalidCredentialsException;
import com.example.atos.exceptions.NotFoundException;
import com.example.atos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class UserService {
    public static int MIN_AGE = 18;
    public static String REQUIRED_COUNTRY = "FRANCE";

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Check the validity of the form
     * @param Register form from client
     * @return boolean
     */
    private boolean isValidFormUser(FormUser formUser) {
        if (isNull(formUser))
            return false;

        return nonNull(formUser.getAge()) && formUser.getAge() >= MIN_AGE &&
                nonNull(formUser.getCountry()) && REQUIRED_COUNTRY.equals(formUser.getCountry().toUpperCase());
    }


    /**
     * Create a user if the FormUser is valid, and save it in the DB, else throw a InvalidCredentialException
     * @param Register form from client
     * @return UserDTO
     * @throws InvalidCredentialsException if form is invalid
     */
    public UserDTO createUser(FormUser formUser) throws InvalidCredentialsException {
        if (!isValidFormUser(formUser)) {
            throw new InvalidCredentialsException("");
        }
        UserDAO userDAO = new UserDAO(formUser);

        userDAO = userRepository.save(userDAO);
        return new UserDTO(userDAO);
    }

    /**
     * Get the complete list of user from mongoDB
     * @return A UserDTO list of all users
     */
    public List<UserDTO> findAllUsers() {
        List<UserDAO> userDAOS = userRepository.findAll();

        return userDAOS.stream().map(UserDTO::new).collect(Collectors.toList());
    }

    /**
     * Find a user by ID, else throw a NotFoundException
     * @param the ID of a user
     * @return A User DTO that describe this user
     * @throws NotFoundException if user does not exist
     */
    public UserDTO findUser(String userId) throws NotFoundException {
        Optional<UserDAO> userDAOOptional = userRepository.findById(userId);

        if (!userDAOOptional.isPresent()) {
            throw new NotFoundException(null);
        }
        return new UserDTO(userDAOOptional.get());
    }
}
